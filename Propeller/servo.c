/**
 * \file servo.c
 * \author Jason Doyle
 * 
 * \brief A simple servo test.
 * 
 */
#include "simpletools.h"                      
#include "servo.h"


/**
 * Runs a continuous rotation servo full speed in one direction for one second, 
 * stops for a second, then spins in the oposite direction for a second.
 */
int main()                                 
{
  
  servo_speed(0, 100);
  pause(1000);
  servo_speed(0, 0);
  pause(1000);
  servo_speed(0, -100);
  pause(1000);
  servo_speed(0, 0);
 
}
